package com.datasol.springbootdemo.service;

import com.datasol.springbootdemo.entity.UserEntity;
import com.datasol.springbootdemo.model.UserSignUpRequest;
import com.datasol.springbootdemo.model.UserSignUpResponse;
import com.datasol.springbootdemo.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserSignUpService {

  @Autowired
  UserRepository userRepository;

  public UserSignUpResponse createUser(UserSignUpRequest userSignUpRequest){
    UserSignUpResponse upResponse = new UserSignUpResponse();
    UserEntity userEntity = new UserEntity();
    BeanUtils.copyProperties(userSignUpRequest, userEntity);
    userRepository.save(userEntity);
    upResponse.setEmail(userEntity.getEmail());
    upResponse.setFirstName(userEntity.getFirstName());
    upResponse.setLastName(userEntity.getLastName());
    upResponse.setDob(userEntity.getDob());
    return upResponse;

  }
  public UserSignUpResponse getUser(Long id){
    UserEntity userEntity = userRepository.findById(id).get();
    UserSignUpResponse userSignUpResponse = new UserSignUpResponse();
    userSignUpResponse.setEmail(userEntity.getEmail());
    userSignUpResponse.setFirstName(userEntity.getFirstName());
    userSignUpResponse.setLastName(userEntity.getLastName());
    userSignUpResponse.setDob(userEntity.getDob());
    return userSignUpResponse;
  }
  public UserSignUpResponse deleteUser(Long id){
    UserEntity userEntity = userRepository.findById(id).get();
    userRepository.delete(userEntity);
    UserSignUpResponse userSignUpResponse = new UserSignUpResponse();
    userSignUpResponse.setEmail(userEntity.getEmail());
    return userSignUpResponse;
  }
  public UserSignUpResponse updateUser(Long id, UserSignUpRequest userSignUpRequest){
    UserEntity userEntity = userRepository.findById(id).get();
    BeanUtils.copyProperties(userSignUpRequest, userEntity);
    userRepository.save(userEntity);
    UserSignUpResponse userSignUpResponse = new UserSignUpResponse();
    userSignUpResponse.setEmail(userEntity.getEmail());
    BeanUtils.copyProperties(userEntity, userSignUpResponse);
    return  userSignUpResponse;
  }
}
