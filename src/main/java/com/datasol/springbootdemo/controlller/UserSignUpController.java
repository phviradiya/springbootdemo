package com.datasol.springbootdemo.controlller;

import com.datasol.springbootdemo.entity.UserEntity;
import com.datasol.springbootdemo.model.UserSignUpRequest;
import com.datasol.springbootdemo.model.UserSignUpResponse;
import com.datasol.springbootdemo.service.UserSignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user-signup")
public class UserSignUpController {
    //REST METHODS
    /*

    POST --> Create a new record
    GET --> Get the existing record
    PUT --> Update
    DELETE --> Delete
    PATCH --> Multiple Records Update
     */
    @Autowired
    UserSignUpService userSignUpService;

    @GetMapping(value = "/get")
    public String getExample() {
        return "Hello GET is called.";
    }

    @PutMapping(value = "/put")
    public String putExample() {
        return "Hello PUT is called.";
    }

    @PostMapping(value = "/post")
    public String postExample() {
        return "Hello POST is called.";
    }

    @PostMapping(value = "/sign-up")
    public UserSignUpResponse userSignUp(@RequestBody UserSignUpRequest userSignUpRequest) {
        UserSignUpResponse upResponse = userSignUpService.createUser(userSignUpRequest);
        return upResponse;
    }
    @GetMapping(value = "/get-user")
    public UserSignUpResponse userDetails(@RequestParam("Id") Long id) {
        return userSignUpService.getUser(id);
    }


    @DeleteMapping(value = "/del-user")
    public UserSignUpResponse deleteUser(@RequestParam("Id") Long id) {
        return userSignUpService.deleteUser(id);
    }
   /* @GetMapping(value = "/get-user")
    public UserEntity userDetails(@PathVariable("Id") Long id) {
        return userSignUpService.getUser(id);
    }

    @DeleteMapping(value = "/delete-user")
    public UserSignUpResponse userSignUp(@RequestBody UserSignUpRequest userSignUpRequest) {
        UserSignUpResponse upResponse = userSignUpService.deleteUser(userSignUpRequest);
        return upResponse;
    }

    @PostMapping(value = "/update-user")
    public UserSignUpResponse userSignUp2(@RequestBody UserSignUpRequest userSignUpRequest) {
        UserSignUpResponse upResponse = userSignUpService.createUser(userSignUpRequest);
        return upResponse;
    }*/


}